package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.size() >= Integer.MAX_VALUE - 1) {
            throw new CannotBuildPyramidException();
        }
        else if (findToHeight(inputNumbers.size())==0) {
            throw new CannotBuildPyramidException();
        } else {
            Collections.sort(inputNumbers);
            int rows = findToHeight(inputNumbers.size());
            int cols = findToHeight(inputNumbers.size()) * 2 - 1;
            int [][] resultArray = new int [rows][cols];
            resultArray[0][rows - 1] = inputNumbers.get(0);
            int n = 1;
            for (int i = 1; i <= (rows - 1); i++) {
                for (int j = 0; j <= i; j++) {
                    resultArray[i][(rows -1) - i + 2 * j] = inputNumbers.get(n);
                    n++;
                }
            }
            return resultArray;
        }
    }

   private int findToHeight(int size) {
       int b = 1;
       int a = 1;
       int c = -2 * size;
       int d = b * b - 4 * a * c;

       Double x = (-b + Math.sqrt(d)) / (2 * a);

       if (x % 1 != 0) throw new CannotBuildPyramidException();

       return x.intValue();
   }
}
