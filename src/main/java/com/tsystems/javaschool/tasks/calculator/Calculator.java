package com.tsystems.javaschool.tasks.calculator;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Locale;

public  class Calculator  {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) throws ArithmeticException {
        Counting calc = new Counting();
        LinkedList<Double> numbers = new LinkedList<>();
        LinkedList<Character> mathOperators = new LinkedList<>();


        if (testOperators(statement)){
            return null;
        }
        try {
            for (int i = 0; i < statement.length(); i++) {
                char sym = statement.charAt(i);
                if (sym == '(') {
                    mathOperators.add(sym);
                } else if (sym == ')') {
                    while (mathOperators.getLast() != '(') {
                        calc.Counting(numbers, mathOperators.removeLast());
                        numbers.add(calc.getResult());
                    }
                    mathOperators.removeLast();
                } else if (sym == '+' || sym == '-' || sym == '/' || sym == '*') {
                    if (!mathOperators.isEmpty() && sequencing(mathOperators.getLast()) >= sequencing(sym)) {
                        calc.Counting(numbers, mathOperators.removeLast());
                        numbers.add(calc.getResult());
                    }
                    mathOperators.add(sym);
                } else {
                    String s = "";
                    while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
                        s += statement.charAt(i++);
                    }
                    --i;
                    numbers.add(Double.parseDouble(s));
                }
            }
            while (!mathOperators.isEmpty()) {
                calc.Counting(numbers, mathOperators.removeLast());
                numbers.add(calc.getResult());
            }
        } catch (ArithmeticException e){
            return null;
        }
        double result = numbers.getLast();

        result = BigDecimal.valueOf(result).setScale(4, BigDecimal.ROUND_HALF_DOWN).doubleValue();

        return format(result);

    }

    public boolean testOperators (String statement){
        if (statement == null || statement.contains(",") ||
                statement.isEmpty() ||  statement.contains("..") ||
                statement.contains("++") || statement.contains("--") ||
                statement.contains("//") || statement.contains("**")){
            return true;
        }
        int countBrace = 0;
        for (char sym : statement.toCharArray()){
            if (sym == '('){
                countBrace++;
            }
            else if(sym == ')'){
                countBrace--;
            }
            if (countBrace != 0){
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private int sequencing(char c){
        if (c == '*' || c == '/'){
            return 1;
        }
        else if (c == '+' || c == '-'){
            return 0;
        }
        else{
            return -1;
        }
    }
    private String format(double d){
        if(d == (long) d)
            return String.format(Locale.US, "%d", (long) d);
        else
            return String.format(Locale.US, "%s", d);
    }
}
