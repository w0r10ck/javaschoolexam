package com.tsystems.javaschool.tasks.calculator;
import java.util.LinkedList;

public class Counting {

    private double result;

    public void Counting (LinkedList<Double> numbers, char mathOperators) {

        double firstNumber = numbers.removeLast();

        double secondNumber = numbers.removeLast();

        switch (mathOperators){
            case '+':

                this.result = (secondNumber+firstNumber);
                break;

            case '-':
                this.result = (secondNumber - firstNumber);
                break;

            case '*':
                this.result = (firstNumber * secondNumber);
                break;

            case '/':
                if (firstNumber == 0) {
                    throw new ArithmeticException();
                }
                else {
                    this.result = (secondNumber/firstNumber);
                    break;
                }
        }
    }
    public double getResult() {
        return result;
    }


}
